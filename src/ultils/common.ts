export const omitEmpty = (params: Record<string, unknown>) =>
  Object.keys(params)
    .filter((k) => params[k] !== undefined)
    .reduce((c, v) => ({ ...c, [v]: params[v] }), {});

  