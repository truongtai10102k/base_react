import { Routes, Route } from "react-router-dom";
import Home from "./Page/Home";
import About from "./Page/About";
import User from "./Page/User";

const Router = () => {
  return (
    <Routes>
      <Route path="/">
        <Route index element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="user" element={<User />} />
      </Route>
    </Routes>
  );
};

export default Router;
