import { pathApi } from "../ultils/pathAPI";
import api, { useAPI } from "./useAPI";

export const useUsers = ({ page = 0, size = 10 }) => {
  const { data, error, loading, mutate } = useAPI<User>(pathApi.user, {
    page,
    size,
  });

  const postUser = async (data: User) => {
    await api.POST(pathApi.user, { ...data }).then(() => {
      mutate();
    });
  };

  return [
    {
      data: data || [],
      error,
      loading,
    },
    { mutate, postUser },
  ] as const;
};
