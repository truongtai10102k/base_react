import { createSearchParams } from "react-router-dom";
import useSWR, { SWRConfiguration } from "swr";
import { omitEmpty } from "../ultils/common";
import { configs } from "../ultils/pathAPI";

export const request = async <T>({
  path = "",
  params,
  options = {} as RequestInit,
}) => {
  let url = configs.baseUrl + path;
  const method = options.method || "GET";
  const headers = new Headers(options.headers);
  let body: BodyInit | undefined;
  if (method === "POST" || method === "PUT") {
    if (params) {
      if (params instanceof FormData) {
        body = params;
      } else {
        body = JSON.stringify(params);
        headers.set("Content-Type", "application/json");
      }
    }
  } else if (params) {
    url += `?${createSearchParams(omitEmpty(params))}`;
  }

  const res = await fetch(url, { ...options, headers, body, method });
  const data = (await res.json()) as Record<string, unknown>;
  if (!res.ok) {
    const err: Error & { code?: number } = new Error(
      `${data?.message || data?.error}`
    );
    err.code = typeof data?.code === "number" ? data?.code : -1;
    throw err;
  }

  return data as T;
};

export function useAPI<T>(
  path?: string | null | false,
  params?: Record<string, unknown>,
  options?: RequestInit,
  swrOptions?: SWRConfiguration
) {
  const { data, isLoading, error, mutate } = useSWR<
    T,
    Error & { code?: number }
  >(
    path ? [{ path, params, options }, "useAPI"] : null,
    ([params]: [Parameters<typeof request>[0]]) => request(params),
    swrOptions || {
      revalidateOnFocus: false,
      revalidateIfStale: false,
      shouldRetryOnError: false,
    }
  );

  return { data, error, loading: isLoading, mutate };
}

const api = {
  GET<T = any>(path = "", params = {}, options = {} as RequestInit) {
    return request<T>({ path, params, options: { ...options, method: "GET" } });
  },
  POST<T = any>(path = "", params = {}, options = {} as RequestInit) {
    return request<T>({
      path,
      params,
      options: { ...options, method: "POST" },
    });
  },
  PUT<T = any>(path = "", params = {}, options = {} as RequestInit) {
    return request<T>({ path, params, options: { ...options, method: "PUT" } });
  },
  DELETE<T = any>(path = "", params = {}, options = {} as RequestInit) {
    return request<T>({
      path,
      params,
      options: { ...options, method: "DELETE" },
    });
  },
};

export default api;
