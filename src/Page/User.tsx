import { Link } from "react-router-dom";
import { useUsers } from "../Hooks/useUser";

const User = () => {
  const [{ data, error, loading }, { postUser }] = useUsers({
    page: 1,
    size: 20,
  });

  return (
    <div>
      <div style={{ marginBottom: 20 }}>
        <Link to={"/"} style={{ marginRight: 20 }}>
          Home
        </Link>
        <Link to={"/about"}>About</Link>
      </div>
      <div>User page</div>

      <div>{loading && "Loading..."}</div>
      <div>{error && "error..."}</div>
      <div>{data && "Data..."}</div>
      
      <button onClick={() => postUser({ gender: "Female", name: "testt" })}>
        postUser
      </button>
    
    </div>
  );
};

export default User;
