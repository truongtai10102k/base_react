import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div>
      <div style={{ marginBottom: 20 }}>
        <Link style={{ marginRight: 20 }} to={"/user"}>
          User
        </Link>
        <Link to={"/about"}>About</Link>
      </div>
      <div>home page</div>
    </div>
  );
};

export default Home;
