import { Link } from "react-router-dom";

const About = () => {
  return (
    <div>
      <div style={{ marginBottom: 20 }}>
        <Link to={"/"} style={{ marginRight: 20 }}>
          Home
        </Link>
        <Link to={"/user"}>User</Link>
      </div>
      <div>About page</div>
    </div>
  );
};

export default About;
