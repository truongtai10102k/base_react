interface User {
  name: string;
  gender: "Male" | "Female";
}
